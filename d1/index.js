// alert("Hello World");
console.log("Hello World")
console.
log
(
	"hello again"
);
/*
Variables
it is used to store data any information that is used by an application is tored in what we called "memory" variables are initialized with use of let/const keyword

variable names should be indicative or descriptive of the value being stored to avoid confusion

camelCasing for multiple words
*/
/*
let myVariable="Hello";

console.log(myVariable);

let firstName = "Michael";
let pokemon = 25000;
let first name = "Michael"
console.log(first name);

let first_name = "Michael"
console.log(first_name);
*/
let productName ="Desktop Computer"
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

console.log(productPrice*interest);
/*
	SYNTAX: letVariableName = newValue;
*/
productName = "Laptop";
console.log(productName);
let friend = "Jason";
console.log(friend);

friend = "Jacob";
console.log(friend);


/*interest = 4.489
console.log(interest);*/

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);
supplier = "Zuitt Store";
console.log(supplier);

a = 5;
console.log(a);
var a;/*without keyword default keyword used is let*/

// scope of variables
let outerVariable = "hello";
{
	let innerVariable = "hello again";
	console.log(innerVariable);
}
console.log(outerVariable);
/*console.log(innerVariable);*/
/*
	multiple variables can be declared in one line usng one keyword 
	the variabl declarations must separated by a comma.
*/
let productCode = "CD017",productBrand = "Dell";
console.log(productCode);
console.log(productBrand);
/*
const let = "hello";
console.log(let);*/

// Data types in javascript
/*{
	strings are series of characters that create a word,prhase,a sentence 
	or anything relate to creating a text.
	concatenating strings

}*/

let country="Philippines";
console.log(country);
let province = "Metro Manila";
console.log(province + ", " +country);

//escape character
// \n\n new line break

let mailAddress = "Metro Manila\n\nPhilippines"; 
console.log(mailAddress);

//using double quotes and singles quotes for string data types are actually valid in js.
console.log("John's employees went home early.");
console.log('John\'s employees went home early.');

// Numbers integers 
let headcount = 26;
console.log(headcount);
//decimal/fractions/
let grade = 98.7;
console.log(grade);

//exponent notation
let planetDistance = 2e10;
console.log(planetDistance);

//combining strings and numbers
console.log("John's grade last quarter is "+grade);

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

console.log(isMarried+"\n"+inGoodConduct);

//boolean normally used to store values relating to the state of certain things true/false
console.log("isMarried "+ isMarried);
console.log(`inGoodConduct ${inGoodConduct}`);

//Array special kinds of data type that are used to store multiple values
//can store different data types but it is normally used to store similar data types
//it is not advisable to use different data types in an array since it would be confusing for other devs when they read our codes.

let grades =[98.7,92.1,94.6,95.1];
console.log(grades);

let person = ["John","Smith",32,true];
console.log(person);

//object data type
//objects are another special kind of data type that's used to mimic real world objects. they are used to create complex data that contains pieces of information that are relevant to each other.
//let/const varName = { }
//
let personDetails = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: true,
	contact: ["09123456789","09987654321"],
	address:{
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(personDetails);

console.log(typeof personDetails);
console.log(typeof isMarried);

const anime = ["Naruto","Slam Dunk","One Piece"];
console.log(anime);

// const does not define a constant value for arrays/objects. it defines a constant reference to a value

anime[0] = ["Akame ga Kill"];
console.log(anime);
anime[1] = ["wakeke","dsfkljls"]
console.log(anime);
anime[2] = "esifsif","fjskdjfij",12334;
console.log(anime);

//null data type
let number = 0;
let string = "";
console.log(number);
console.log(string);

let jowa = null;
console.log(jowa);
// null is used intentionally express the absence of a value inside a variable in a declaration/initialization.

