let firstName = "Joseph";
console.log("First Name: "+ firstName);
let lastName = "Concillado";
console.log("Last Name: "+ lastName);
let myAge = 33;
console.log("Age: "+ myAge);
let hobbies = ["Trading Crypto/Forex","Watching Movies","Playing Table Tennis"];
console.log("Hobbies:");
console.log(hobbies);
let workAddress = {
    houseNumber: "12",
    street: "Balford Ave.",
    city: "Scarborough",
    state: "Ontario"
};

console.log("Work Address:");
console.log(workAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let age = 40;
console.log("My current age is: " + age);
	
let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false

};
console.log("My Full Profile: ")
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
lastLocation[0] = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);
